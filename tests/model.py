import datetime

class GenericError(Exception):
    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super().__init__(message)

class User():
    def __init__(self, name:str, id:str, role: str):
        self.is_active:int = 1
        self.role:str = role
        self.name:str = name
        self.id:str = id
        self.password: str = ""
    def set_password(self, password:str):
        self.password=password
    def get_password(self) -> str:
        return self.password
    def set_role(self, role:str):
        self.role=role
    def get_role(self) -> str:
        return self.role
    def deactivate(self):
        self.is_active = 0
    def set_name(self, name:str):
        self.name = name
    def get_name(self) -> str:
        return self.name
    def get_active_status(self) -> int:
        return self.is_active
    def set_id(self, id:str):
        self.id = id
    def get_id(self):
        return self.id

class Student():
    def __init__(self, name: str, birth_date: datetime.datetime, identity_document_number: str, id: str):
        self.name: str = name
        self.id: str = id
        self.birth_date: datetime = birth_date
        self.identity_document_number: str = identity_document_number
    def set_id(self, id: str):
        self.id = id
    def get_id(self) -> str:
        return self.id
    def set_birth_date(self, birth_date: datetime):
        self.birth_date = birth_date
    def get_birth_date(self) -> datetime:
        return self.birth_date
    def set_identity_document_number(self, identity_document_number):
        self.identity_document_number = identity_document_number
    def get_identity_document_number(self):
        return self.identity_document_number
    def set_name(self, name: str):
        self.name = name
    def get_name(self) -> str:
        return self.name


class StudentsGroup():
    def __init__(self, name:str, id:str, max_num_of_students:int):
        self.id:str=id
        self.name:str=name
        self.students:list = []
        self.max_num_of_students: int = max_num_of_students
    def get_students(self) -> list:
        return self.students
    def add_student(self, student: Student):
        for k, v in enumerate(self.students):
            if v.get_id() == student.get_id() or v.get_identity_document_number() == student.get_identity_document_number():
                raise GenericError("Student with given id or identity document number already exists in this group")
                return
        if len(self.students) < self.max_num_of_students:
            self.students.append(student)
        else:
            raise GenericError("This group size reached maximum number of students")
    def exclude_student(self, student: Student):
        self.students.remove(student)
    def set_id(self, id: str):
        self.id = id
    def get_id(self) -> str:
        return self.id
    def set_name(self, name: str):
        self.name = name
    def get_name(self) -> str:
        return self.name
    def find_student(self, **kwargs):
        out_student: Student = None
        if "student_id" in kwargs:
            for i, student in enumerate(self.students):
                if student.get_id() == student_id:
                    out_student = student
                    break
        elif "identity_document_number" in kwargs:
            for i, student in enumerate(self.students):
                if student.get_identity_document_number() == kwargs["identity_document_number"]:
                    out_student = student
                    break
        return out_student
    def set_department_id(self, department_id: str):
        self.department_id = department_id
    def get_department_id(self) -> str:
        return self.department_id

class Department():
    def __init__(self, name, id, material_fund_size_per_group:int, min_payment_per_student:int):
        self.name = name
        self.id = id
        self.groups:list=[]
        self.users:list=[]
        self.material_fund_size_per_group:int=material_fund_size_per_group
        self.min_payment_per_student:int=min_payment_per_student
    def add_group(self, group: StudentsGroup):
        for k,v in enumerate(self.groups):
            if v.get_id() == group.get_id() or v.get_name() == group.get_name():
                raise GenericError("Group with given id or name already exists in this department")
                return
        self.groups.append(group)
    def remove_group(self, group: StudentsGroup):
        self.groups.remove(group)
    def get_groups(self) -> list:
        return self.groups
    def find_group(self, group_id:str) -> StudentsGroup:
        out_group: StudentsGroup = None
        for i, group in enumerate(self.groups):
            if group.id == group_id:
                out_group = group
        return out_group

    def find_group_not_full(self) -> StudentsGroup:
        out_group: StudentsGroup = None
        for i, group in enumerate(self.groups):
            if len(group.get_students()) < group.max_num_of_students:
                out_group = group
                break
        return out_group

    def add_user(self, user: User):
        for k,v in enumerate(self.users):
            if v.get_id() == user.get_id():
                raise GenericError("User with given id already exists in this department")
                return
        self.users.append(user)
    def remove_user(self, user):
        self.users.remove(user)
    def find_user(self, user_id: str) -> User:
        for i, user in enumerate(self.users):
            if user.id == user_id:
                return user
    def get_users(self) -> list:
        return self.users
    def set_id(self,id: str):
        self.id=id
    def get_id(self) -> str:
        return self.id
    def set_name(self, name:str):
        self.name=name
    def get_name(self) -> str:
        return self.name
    def set_material_fund_size_per_group(self, material_fund_size_per_group: int):
        self.material_fund_size_per_group = material_fund_size_per_group
    def get_material_fund_size_per_group(self) -> int:
        return self.material_fund_size_per_group
    def set_min_payment_per_student(self, min_payment_per_student: int):
        self.min_payment_per_student = min_payment_per_student
    def get_min_payment_per_student(self) -> int:
        return self.min_payment_per_student

class StudentHistory(Student):
    def __init__(self, student: Student, date_archived):
        super().__init__(
            name=student.name,
            id = student.id,
            birth_date = student.birth_date,
            identity_document_number = student.identity_document_number
        )
        self.date_archived:datetime = date_archived
    def set_date_archived(self, date_archived: datetime):
        self.date_archived = date_archived
    def get_date_archived(self) -> datetime:
        return self.date_archived

class Archive():
    def __init__(self):
        self.students_history: list = []
    def add_student(self, student: Student):
        self.students_history.append(StudentHistory(student=student, date_archived=datetime.datetime.now().replace(hour=0,minute=0,second=0,microsecond=0)))
    def find_archived_student(self, **kwargs) -> StudentHistory:
        out_student: StudentHistory = None
        if "student_id" in kwargs and "date_archived" in kwargs:
            for i, student in enumerate(self.students_history):
                if student.get_id() == kwargs["student_id"] and student.get_date_archived() == kwargs["date_archived"]:
                    out_student = student
                    break
        elif "identity_document_number" in kwargs and "date_archived" in kwargs:
            for i, student in enumerate(self.students_history):
                if student.get_identity_document_number() == kwargs["identity_document_number"] and student.date_archived == kwargs["date_archived"]:
                    out_student = student
                    break
        elif "student_id" in kwargs:
            for i, student in enumerate(self.students_history):
                if student.get_id() == kwargs["student_id"]:
                    if out_student == None:
                        out_student = student
                    elif out_student.get_date_archived() < student.get_date_archived():
                        out_student = student
        elif "identity_document_number" in kwargs:
            for i, student in enumerate(self.students_history):
                if student.get_identity_document_number() == kwargs["identity_document_number"]:
                    if out_student == None:
                        out_student = student
                    elif out_student.get_date_archived() < student.get_date_archived():
                        out_student = student
        else:
            raise GenericError("At least student_id or identity_document_number arguments are required")
        return out_student
