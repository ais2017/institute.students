import time
import unittest
import datetime
from model import Department, StudentsGroup, Student, Archive, User, StudentHistory

class ModelTest(unittest.TestCase):

    def setUp(self):
        self.user = User(
            name="Test",
            id="1",
            role="Professor"
        )
        self.department = Department(
            name="IIKS",
            id="1",
            material_fund_size_per_group=20,
            min_payment_per_student=10
        )
        self.group = StudentsGroup(
            id="1",
            name="M18-512",
            max_num_of_students=self.department.material_fund_size_per_group // self.department.min_payment_per_student
            #max_num_of_students=1
        )
        self.student = Student(
            name="Ilya",
            birth_date=datetime.datetime(1999, 10, 5),
            identity_document_number="12345",
            id="1"
        )
        self.archive = Archive()
        self.group.add_student(self.student)
        self.department.add_group(self.group)
        self.department.add_user(self.user)
        self.student_history = StudentHistory(
            student=self.student,
            date_archived=datetime.datetime.now()
        )

    def tearDown(self):
        self.user = None
        self.department = None
        self.group = None
        self.student = None
        self.student_history = None
        self.archive = None

    def test_user_set_and_get_role(self):
        self.user.set_role("some role")
        self.assertEqual(self.user.get_role(), "some role")
    def test_user_deactivate(self):
        self.user.deactivate()
        self.assertEqual(self.user.get_active_status(), 0)

    def test_department_add_group(self):
        self.department.add_group(StudentsGroup(
            name="Group2",
            id="2",
            max_num_of_students=self.department.get_material_fund_size_per_group() // self.department.get_min_payment_per_student()
        ))
        self.assertIsInstance(self.department.find_group("2"), StudentsGroup)
    def test_department_remove_group(self):
        self.department.remove_group(self.group)
        self.assertIsNone(self.department.find_group(self.group.get_id()))
    def test_department_get_groups(self):
        self.assertEqual(len(self.department.get_groups()), 1)
    def test_department_find_group(self):
        self.assertIsNotNone(self.department.find_group(group_id="1"))
    def test_department_find_group_not_full(self):
        self.assertIsNotNone(self.department.find_group_not_full())
    def test_department_add_user(self):
        self.department.add_user(User(
            name="tet1",
            id="2",
            role="test role"
        ))
        self.assertIsNotNone(self.department.find_user(user_id="2"))
    def test_department_remove_user(self):
        self.department.remove_user(self.user)
        self.assertIsNone(self.department.find_user(self.user.get_id()))
    def test_department_find_user(self):
        self.assertIsNotNone(self.department.find_user(user_id="1"))
    def test_department_get_users(self):
        self.assertEqual(len(self.department.get_users()), 1)

    def test_group_get_students(self):
        self.assertEqual(len(self.group.get_students()), 1)
    def test_group_add_student(self):
        self.group.add_student(Student(
            name="abc",
            id="2",
            birth_date=datetime.datetime(1989,10,1),
            identity_document_number="123456"
        ))
        self.assertIsNotNone(self.group.find_student(identity_document_number=self.student.get_identity_document_number()))
    def test_group_exclude_student(self):
        self.assertIsNotNone(self.group.find_student(identity_document_number=self.student.get_identity_document_number()))
        self.group.exclude_student(student=self.student)
        self.assertIsNone(self.group.find_student(identity_document_number=self.student.get_identity_document_number()))
    def test_group_find_student(self):
        self.assertIsInstance(self.group.find_student(identity_document_number="12345"), Student)
        self.assertIsNone(self.group.find_student(identity_document_number="00"))

    def test_student_history(self):
        date = datetime.datetime.now()
        self.student_history.set_date_archived(date_archived=date)
        self.assertEqual(self.student_history.get_date_archived(), date)

    def test_archive(self):
        self.archive.add_student(self.student)
        self.assertIsInstance(self.archive.find_archived_student(student_id="1"), StudentHistory)
        self.assertIsInstance(self.archive.find_archived_student(student_id="1", date_archived=datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)), StudentHistory)


if __name__ == '__main__':
    unittest.main()
