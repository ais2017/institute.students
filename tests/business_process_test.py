import unittest
import datetime
from model import Department, StudentsGroup, Student, Archive, User

class ModelTest(unittest.TestCase):

    def setUp(self):
        self.user = User(
            name="Test",
            id="321",
            role="Professor"
        )
        self.department = Department(
            name="IIKS",
            id="1",
            material_fund_size_per_group=20,
            min_payment_per_student=10
        )
        self.group = StudentsGroup(
            id="1",
            name="M18-512",
            max_num_of_students=self.department.get_material_fund_size_per_group() // self.department.get_min_payment_per_student()
        )
        self.student = Student(
            name="Ilya",
            birth_date=datetime.datetime(1999, 10, 5),
            identity_document_number="12345",
            id="1"
        )
        self.archive = Archive()
        self.group.add_student(self.student)
        self.department.add_group(self.group)
        self.department.add_user(self.user)

    def tearDown(self):
        self.user = None
        self.department = None
        self.group = None
        self.student = None
        self.student_history = None
        self.archive = None

    def test_group_can_be_removed(self):
        new_group = StudentsGroup(
            id="2",
            name="M18-522",
            max_num_of_students=self.department.get_material_fund_size_per_group() // self.department.get_min_payment_per_student()
        )
        self.department.add_group(new_group)
        for i, student in enumerate(self.group.get_students()):
            new_group.add_student(student)
            self.group.exclude_student(student)
        self.department.remove_group(self.group)
        self.assertEqual(len(self.group.get_students()), 0)
        self.assertEqual(len(new_group.get_students()), 1)
        self.assertEqual(self.department.find_group("1"), None)

    def test_group_can_be_created(self):
        self.assertIsInstance(self.group.find_student(identity_document_number=self.student.get_identity_document_number()), Student)

    def test_exclude_student_from_group(self):
        self.group.exclude_student(self.student)
        self.archive.add_student(self.student)
        self.assertEqual(self.group.find_student(identity_document_number=self.student.get_identity_document_number()), None)
        self.assertEqual(self.archive.find_archived_student(identity_document_number="12345").get_id(), "1")

    def test_group_can_be_changed(self):
        new_group = StudentsGroup(
            id="2",
            name="M18-522",
            max_num_of_students=self.department.get_material_fund_size_per_group() // self.department.get_min_payment_per_student()
        )
        self.assertEqual(len(self.department.get_groups()), 1)
        self.department.add_group(new_group)
        self.assertEqual(len(self.department.get_groups()), 2)
        self.department.find_group(group_id="1").exclude_student(student=self.student)
        self.department.find_group(group_id="2").add_student(student=self.student)
        self.assertEqual(self.department.find_group(group_id="1").find_student(identity_document_number=self.student.get_identity_document_number()), None)
        self.assertEqual(self.department.find_group(group_id="2").find_student(identity_document_number=self.student.get_identity_document_number()), self.student)

    def test_recover_student(self):
        self.archive.add_student(student=self.student)
        self.group.exclude_student(student=self.student)
        self.assertIs(self.group.find_student(identity_document_number=self.student.get_identity_document_number()), None)
        self.assertEqual(self.archive.find_archived_student(identity_document_number=self.student.get_identity_document_number()).get_id(), "1")
        self.department.find_group_not_full().add_student(student=self.archive.find_archived_student(student_id=self.student.id))
        self.assertEqual(self.group.find_student(identity_document_number=self.student.get_identity_document_number()), self.archive.find_archived_student(student_id=self.student.id))

    def test_get_students_list(self):
        self.assertEqual(self.department.find_group(group_id="1").get_students()[0].get_name(), "Ilya")

    def test_user_role_change(self):
        self.user.set_role("new role name")
        self.assertEqual(self.user.role, "new role name")

    def test_create_user(self):
        user = User(
            name="Test1",
            id="321",
            role="Professor"
        )
        self.assertEqual(user.get_name(), "Test1")

    def test_delete_user(self):
        self.user.deactivate()
        self.assertEqual(self.user.get_active_status(), 0)
        self.department.remove_user(self.user)
        self.assertEqual(self.department.find_user(self.user.id), None)

if __name__ == '__main__':
    unittest.main()
